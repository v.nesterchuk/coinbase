package com.v.nesterchuk.coinbase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

@Component
public class CoinbaseWebSocketListener extends WebSocketListener {

    private final OkHttpClient okHttpClient;
    private final InstrumentsRepository repository;
    private final ApplicationContext appContext;

    private final String[] DEFAULT_PRODUCT_IDS = new String[]{"ETH-USD", "ETH-EUR", "BTC-USD", "BTC-EUR"};
    private final int RECONNECT_TRIES = 3;

    private final String[] productsId;
    private final AtomicBoolean isReconnecting = new AtomicBoolean(false);

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    public CoinbaseWebSocketListener(
            OkHttpClient okHttpClient,
            InstrumentsRepository repository,
            ApplicationArguments args,
            ApplicationContext appContext
    ) {
        this.okHttpClient = okHttpClient;
        this.repository = repository;
        this.appContext = appContext;
        String[] sourceArgs = args.getSourceArgs();
        if (sourceArgs.length != 0) {
            this.productsId = sourceArgs;
        } else {
            this.productsId = DEFAULT_PRODUCT_IDS;
        }
        openConnection();
    }

    private void openConnection() {
        logger.info("Opening connection to Coinbase");
        Request request = new Request.Builder()
                .url("wss://ws-feed.exchange.coinbase.com")
                .build();
        okHttpClient.newWebSocket(request, this);
    }

    @Override
    public void onOpen(WebSocket webSocket, @NotNull Response response) {
        isReconnecting.set(false);
        logger.info("Connection with Coinbase established");
        SubscriptionMessage subscriptionMessage = new SubscriptionMessage(productsId);
        try {
            webSocket.send(subscriptionMessage.toJson());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
        if (text.startsWith("{\"type\":\"ticker\"")) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                TickerMessage tickerMessage = objectMapper.readValue(text, TickerMessage.class);
                Instrument instrument = tickerMessage.toInstrument();
                repository.putInstrument(instrument);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, @NotNull String reason) {
        webSocket.close(1000, null);
        reconnect();
    }

    @Override
    public void onFailure(@NotNull WebSocket webSocket, Throwable t, Response response) {
        t.printStackTrace();
        webSocket.close(1000, null);
        reconnect();
    }

    private void reconnect() {
        if (isReconnecting.get()) {
            return;
        } else {
            isReconnecting.set(true);
        }
        logger.warning("Connection to Coinbase was lost. Trying to reconnect");
        for (int i = 0; i < RECONNECT_TRIES && isReconnecting.get(); i++) {
            logger.warning("Reconnect try: " + (i + 1) + "/" + RECONNECT_TRIES);
            openConnection();
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (isReconnecting.get()) {
            logger.warning("Unable to reconnect to Coinbase. Shutting down.");
            int exitCode = SpringApplication.exit(appContext, () -> -1);
            System.exit(exitCode);
        }
    }
}
