package com.v.nesterchuk.coinbase;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class TickerMessage {
    @JsonProperty("type")
    private String type;
    @JsonProperty("sequence")
    private Long sequence;
    @JsonProperty("product_id")
    private String productId;
    private Double price;
    @JsonProperty("open_24h")
    private Double open24h;
    @JsonProperty("volume_24h")
    private Double volume24h;
    @JsonProperty("low_24h")
    private Double low24h;
    @JsonProperty("high_24h")
    private Double high24h;
    @JsonProperty("volume_30d")
    private Double volume30d;
    @JsonProperty("best_bid")
    private Double bestBid;
    @JsonProperty("best_ask")
    private Double bestAsk;
    @JsonProperty("side")
    private String side;
    @JsonProperty("time")
    private String time;
    @JsonProperty("trade_id")
    private Long tradeId;
    @JsonProperty("last_size")
    private Double lastSize;

    public TickerMessage() {
    }

    public TickerMessage(String type, Long sequence, String productId, Double price, Double open24h, Double volume24h, Double low24h, Double high24h, Double volume30d, Double bestBid, Double bestAsk, String side, String time, Long tradeId, Double lastSize) {
        this.type = type;
        this.sequence = sequence;
        this.productId = productId;
        this.price = price;
        this.open24h = open24h;
        this.volume24h = volume24h;
        this.low24h = low24h;
        this.high24h = high24h;
        this.volume30d = volume30d;
        this.bestBid = bestBid;
        this.bestAsk = bestAsk;
        this.side = side;
        this.time = time;
        this.tradeId = tradeId;
        this.lastSize = lastSize;
    }

    Instrument toInstrument() {
        String instrument = productId.replace("-","");
        Double bid = bestBid;
        Double ask = bestAsk;
        Double last = price;
        String formattedTime = getFormattedTime();
        return new Instrument(instrument, bid, ask, last, formattedTime);
    }

    String getFormattedTime() throws IllegalArgumentException{
        if (!time.contains("T") || !time.contains(".")){
            throw new IllegalArgumentException("Not valid time format");
        }
        String timeWithMillis = time.substring(time.indexOf("T") + 1);
        return timeWithMillis.substring(0, timeWithMillis.indexOf("."));
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getOpen24h() {
        return open24h;
    }

    public void setOpen24h(Double open24h) {
        this.open24h = open24h;
    }

    public Double getVolume24h() {
        return volume24h;
    }

    public void setVolume24h(Double volume24h) {
        this.volume24h = volume24h;
    }

    public Double getLow24h() {
        return low24h;
    }

    public void setLow24h(Double low24h) {
        this.low24h = low24h;
    }

    public Double getHigh24h() {
        return high24h;
    }

    public void setHigh24h(Double high24h) {
        this.high24h = high24h;
    }

    public Double getVolume30d() {
        return volume30d;
    }

    public void setVolume30d(Double volume30d) {
        this.volume30d = volume30d;
    }

    public Double getBestBid() {
        return bestBid;
    }

    public void setBestBid(Double bestBid) {
        this.bestBid = bestBid;
    }

    public Double getBestAsk() {
        return bestAsk;
    }

    public void setBestAsk(Double bestAsk) {
        this.bestAsk = bestAsk;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getTradeId() {
        return tradeId;
    }

    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public Double getLastSize() {
        return lastSize;
    }

    public void setLastSize(Double lastSize) {
        this.lastSize = lastSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TickerMessage tickerMessage = (TickerMessage) o;
        return type.equals(tickerMessage.type) &&
                sequence.equals(tickerMessage.sequence) &&
                productId.equals(tickerMessage.productId) &&
                price.equals(tickerMessage.price) &&
                open24h.equals(tickerMessage.open24h) &&
                volume24h.equals(tickerMessage.volume24h) &&
                low24h.equals(tickerMessage.low24h) &&
                high24h.equals(tickerMessage.high24h) &&
                volume30d.equals(tickerMessage.volume30d) &&
                bestBid.equals(tickerMessage.bestBid) &&
                bestAsk.equals(tickerMessage.bestAsk) &&
                side.equals(tickerMessage.side) &&
                time.equals(tickerMessage.time) &&
                tradeId.equals(tickerMessage.tradeId) &&
                lastSize.equals(tickerMessage.lastSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, sequence, productId, price, open24h, volume24h, low24h, high24h, volume30d, bestBid, bestAsk, side, time, tradeId, lastSize);
    }

    @Override
    public String toString() {
        return "TickerMessage{" +
                "type='" + type + '\'' +
                ", sequence=" + sequence +
                ", productId='" + productId + '\'' +
                ", price=" + price +
                ", open24h=" + open24h +
                ", volume24h=" + volume24h +
                ", low24h=" + low24h +
                ", high24h=" + high24h +
                ", volume30d=" + volume30d +
                ", bestBid=" + bestBid +
                ", bestAsk=" + bestAsk +
                ", side='" + side + '\'' +
                ", time='" + time + '\'' +
                ", tradeId=" + tradeId +
                ", lastSize=" + lastSize +
                '}';
    }
}
