package com.v.nesterchuk.coinbase;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Repository
class InstrumentsRepository {
    private final ConcurrentHashMap<String, Instrument> instrumentsToName = new ConcurrentHashMap<>();

    void putInstrument(Instrument instrument) {
        instrumentsToName.put(instrument.getName(), instrument);
    }

    List<Instrument> getAllInstruments(){
        return new ArrayList<>(instrumentsToName.values());
    }

    void clear(){
        instrumentsToName.clear();
    }
}
