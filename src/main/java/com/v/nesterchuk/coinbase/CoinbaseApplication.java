package com.v.nesterchuk.coinbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoinbaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoinbaseApplication.class, args);
	}

}
