package com.v.nesterchuk.coinbase;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.Objects;

public class SubscriptionMessage {
    @JsonProperty("type")
    private String type;
    @JsonProperty("product_ids")
    private String[] productIds;
    @JsonProperty("channels")
    private String[] channels;

    public SubscriptionMessage() {
    }

    public SubscriptionMessage(String[] productIds) {
        this.productIds = productIds;
        type = "subscribe";
        channels = new String[]{"ticker"};
    }

    public SubscriptionMessage(String type, String[] productIds, String[] channels) {
        this.type = type;
        this.productIds = productIds;
        this.channels = channels;
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getProductIds() {
        return productIds;
    }

    public void setProductIds(String[] productIds) {
        this.productIds = productIds;
    }

    public String[] getChannels() {
        return channels;
    }

    public void setChannels(String[] channels) {
        this.channels = channels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionMessage that = (SubscriptionMessage) o;
        return type.equals(that.type) &&
                Arrays.equals(productIds, that.productIds) &&
                Arrays.equals(channels, that.channels);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(type);
        result = 31 * result + Arrays.hashCode(productIds);
        result = 31 * result + Arrays.hashCode(channels);
        return result;
    }

    @Override
    public String toString() {
        return "SubscriptionMessage{" +
                "type='" + type + '\'' +
                ", productIds=" + Arrays.toString(productIds) +
                ", channels=" + Arrays.toString(channels) +
                '}';
    }
}
