package com.v.nesterchuk.coinbase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/instruments")
public class InstrumentsRestController {

    private final InstrumentsRepository repository;

    @Autowired
    public InstrumentsRestController(InstrumentsRepository repository) {
        this.repository = repository;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<Instrument>> getNewestInstrumentsData() {
        return ResponseEntity.ok(repository.getAllInstruments());
    }
}
