package com.v.nesterchuk.coinbase;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Instrument {
    @JsonProperty("instrument")
    private String name;
    @JsonProperty("bid")
    private Double bid;
    @JsonProperty("ask")
    private Double ask;
    @JsonProperty("last")
    private Double last;
    @JsonProperty("time")
    private String time;

    public Instrument() {
    }

    public Instrument(String name, Double bid, Double ask, Double last, String time) {
        this.name = name;
        this.bid = bid;
        this.ask = ask;
        this.last = last;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBid() {
        return bid;
    }

    public void setBid(Double bid) {
        this.bid = bid;
    }

    public Double getAsk() {
        return ask;
    }

    public void setAsk(Double ask) {
        this.ask = ask;
    }

    public Double getLast() {
        return last;
    }

    public void setLast(Double last) {
        this.last = last;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrument that = (Instrument) o;
        return name.equals(that.name) &&
                bid.equals(that.bid) &&
                ask.equals(that.ask) &&
                last.equals(that.last) &&
                time.equals(that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, bid, ask, last, time);
    }

    @Override
    public String toString() {
        return "Instrument{" +
                "name='" + name + '\'' +
                ", bid=" + bid +
                ", ask=" + ask +
                ", last=" + last +
                ", time='" + time + '\'' +
                '}';
    }
}
