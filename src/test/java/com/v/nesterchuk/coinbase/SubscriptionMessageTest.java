package com.v.nesterchuk.coinbase;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SubscriptionMessageTest {

    @Test
    void toJsonEmpty() throws JsonProcessingException {
        SubscriptionMessage subscriptionMessage = new SubscriptionMessage();
        String json = subscriptionMessage.toJson();
        String expected = "{\"type\":null,\"product_ids\":null,\"channels\":null}";
        assertEquals(expected, json);
    }

    @Test
    void toJsonEmptyProducts() throws JsonProcessingException {
        SubscriptionMessage subscriptionMessage = new SubscriptionMessage(new String[0]);
        String json = subscriptionMessage.toJson();
        String expected = "{\"type\":\"subscribe\",\"product_ids\":[],\"channels\":[\"ticker\"]}";
        assertEquals(expected, json);
    }

    @Test
    void toJsonCorrect() throws JsonProcessingException {
        SubscriptionMessage subscriptionMessage = new SubscriptionMessage(new String[]{"ETH-USD","BTC-USD"});
        String json = subscriptionMessage.toJson();
        String expected = "{\"type\":\"subscribe\",\"product_ids\":[\"ETH-USD\",\"BTC-USD\"],\"channels\":[\"ticker\"]}";
        assertEquals(expected, json);
    }

    @Test
    void toJsonFull() throws JsonProcessingException {
        SubscriptionMessage subscriptionMessage = new SubscriptionMessage("unsubscribe",new String[]{"ETH-USD","BTC-USD"}, new String[]{"heartbeat"});
        String json = subscriptionMessage.toJson();
        String expected = "{\"type\":\"unsubscribe\",\"product_ids\":[\"ETH-USD\",\"BTC-USD\"],\"channels\":[\"heartbeat\"]}";
        assertEquals(expected, json);
    }
}