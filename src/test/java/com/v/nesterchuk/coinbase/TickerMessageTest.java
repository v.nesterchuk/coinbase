package com.v.nesterchuk.coinbase;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TickerMessageTest {

    @Test
    void toInstrumentEmpty() {
        TickerMessage message = new TickerMessage();
        assertThrows(NullPointerException.class, message::toInstrument);
    }

    @Test
    void toInstrumentFilled() {
        TickerMessage message = new TickerMessage("ticker", 30104207340L, "BTC-USD", 56119.37, 57286.52, 12817.57051658, 55950.0, 57833.23, 406550.1091376, 56119.36, 56119.37, "buy", "2021-10-12T16:29:22.368947Z", 220376717L, 0.01808036);
        Instrument generated = message.toInstrument();
        Instrument expected = new Instrument("BTCUSD", 56119.36, 56119.37, 56119.37, "16:29:22");
        assertEquals(expected, generated);
    }

    @Test
    void getFormattedTimeEmpty() {
        TickerMessage message = new TickerMessage();
        assertThrows(NullPointerException.class, message::getFormattedTime);
    }

    @Test
    void getFormattedTimeIncorrect() {
        TickerMessage message = new TickerMessage();
        String definitelyNotTime = "DefinitelyNotTime";
        message.setTime(definitelyNotTime);
        assertThrows(IllegalArgumentException.class, message::getFormattedTime);
    }

    @Test
    void getFormattedTimeCorrect() {
        TickerMessage message = new TickerMessage();
        String correctTime = "2021-10-12T16:29:22.368947Z";
        message.setTime(correctTime);
        String generated = message.getFormattedTime();
        String expected = "16:29:22";
        assertEquals(expected,generated);
    }
}