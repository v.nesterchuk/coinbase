package com.v.nesterchuk.coinbase;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InstrumentsRepositoryTest {

    @Test
    void putInstrumentCorrect() {
        InstrumentsRepository instrumentsRepository = new InstrumentsRepository();
        Instrument firstInstrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        Instrument secondInstrument = new Instrument("Name2", 2.0, 0.2, 2.0, "22:22:22");
        Instrument thirdInstrument = new Instrument("Name3", 3.0, 0.3, 3.0, "23:23:23");
        instrumentsRepository.putInstrument(firstInstrument);
        instrumentsRepository.putInstrument(secondInstrument);
        instrumentsRepository.putInstrument(thirdInstrument);
        List<Instrument> allInstruments = instrumentsRepository.getAllInstruments();

        assertFalse(allInstruments.isEmpty());
        assertEquals(3, allInstruments.size());
        assertTrue(allInstruments.contains(firstInstrument));
        assertTrue(allInstruments.contains(secondInstrument));
        assertTrue(allInstruments.contains(thirdInstrument));
    }

    @Test
    void putInstrumentOverride() {
        InstrumentsRepository instrumentsRepository = new InstrumentsRepository();
        Instrument firstInstrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        instrumentsRepository.putInstrument(firstInstrument);

        List<Instrument> allInstruments = instrumentsRepository.getAllInstruments();

        assertFalse(allInstruments.isEmpty());
        assertEquals(1, allInstruments.size());
        assertTrue(allInstruments.contains(firstInstrument));

        Instrument secondInstrument = new Instrument("Name1", 100.0, 0.100, 100.0, "12:12:12");
        instrumentsRepository.putInstrument(secondInstrument);
        allInstruments = instrumentsRepository.getAllInstruments();

        assertFalse(allInstruments.isEmpty());
        assertEquals(1, allInstruments.size());
        assertFalse(allInstruments.contains(firstInstrument));
        assertTrue(allInstruments.contains(secondInstrument));
    }

    @Test
    void getAllInstrumentsEmpty() {
        InstrumentsRepository instrumentsRepository = new InstrumentsRepository();
        List<Instrument> allInstruments = instrumentsRepository.getAllInstruments();
        assertTrue(allInstruments.isEmpty());
    }

    @Test
    void getAllInstrumentsCorrect() {
        InstrumentsRepository instrumentsRepository = new InstrumentsRepository();
        Instrument firstInstrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        Instrument secondInstrument = new Instrument("Name2", 2.0, 0.2, 2.0, "22:22:22");
        Instrument thirdInstrument = new Instrument("Name3", 3.0, 0.3, 3.0, "23:23:23");
        instrumentsRepository.putInstrument(firstInstrument);
        instrumentsRepository.putInstrument(secondInstrument);
        instrumentsRepository.putInstrument(thirdInstrument);
        List<Instrument> allInstruments = instrumentsRepository.getAllInstruments();

        assertFalse(allInstruments.isEmpty());
        assertEquals(3, allInstruments.size());
        assertTrue(allInstruments.contains(firstInstrument));
        assertTrue(allInstruments.contains(secondInstrument));
        assertTrue(allInstruments.contains(thirdInstrument));
    }

    @Test
    void getAllInstrumentsOverride() {
        InstrumentsRepository instrumentsRepository = new InstrumentsRepository();
        Instrument firstInstrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        instrumentsRepository.putInstrument(firstInstrument);

        List<Instrument> allInstruments = instrumentsRepository.getAllInstruments();

        assertFalse(allInstruments.isEmpty());
        assertEquals(1, allInstruments.size());
        assertTrue(allInstruments.contains(firstInstrument));

        Instrument secondInstrument = new Instrument("Name1", 100.0, 0.100, 100.0, "12:12:12");
        instrumentsRepository.putInstrument(secondInstrument);
        allInstruments = instrumentsRepository.getAllInstruments();

        assertFalse(allInstruments.isEmpty());
        assertEquals(1, allInstruments.size());
        assertFalse(allInstruments.contains(firstInstrument));
        assertTrue(allInstruments.contains(secondInstrument));
    }

    @Test
    void clear(){
        InstrumentsRepository instrumentsRepository = new InstrumentsRepository();
        Instrument firstInstrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        Instrument secondInstrument = new Instrument("Name2", 2.0, 0.2, 2.0, "22:22:22");
        Instrument thirdInstrument = new Instrument("Name3", 3.0, 0.3, 3.0, "23:23:23");
        instrumentsRepository.putInstrument(firstInstrument);
        instrumentsRepository.putInstrument(secondInstrument);
        instrumentsRepository.putInstrument(thirdInstrument);
        List<Instrument> allInstruments = instrumentsRepository.getAllInstruments();

        assertFalse(allInstruments.isEmpty());
        assertEquals(3, allInstruments.size());
        assertTrue(allInstruments.contains(firstInstrument));
        assertTrue(allInstruments.contains(secondInstrument));
        assertTrue(allInstruments.contains(thirdInstrument));

        instrumentsRepository.clear();
        allInstruments = instrumentsRepository.getAllInstruments();

        assertTrue(allInstruments.isEmpty());
    }
}