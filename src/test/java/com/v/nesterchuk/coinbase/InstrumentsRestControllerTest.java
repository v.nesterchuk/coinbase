package com.v.nesterchuk.coinbase;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class InstrumentsRestControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    InstrumentsRepository repository;
    @MockBean
    CoinbaseWebSocketListener socketListener;

    @BeforeEach
    void clearRepository(){
        repository.clear();
    }

    @Test
    void getNewestInstrumentsDataEmpty() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/instruments")).andExpect(status().isOk()).andReturn();
        assertEquals("[]",mvcResult.getResponse().getContentAsString());
    }

    @Test
    void getNewestInstrumentsDataOne() throws Exception {
        Instrument instrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        repository.putInstrument(instrument);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/instruments")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String expected = mapper.writeValueAsString(List.of(instrument));
        assertEquals(expected,mvcResult.getResponse().getContentAsString());
    }
    @Test
    void getNewestInstrumentsDataMultiple() throws Exception {
        Instrument instrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        Instrument secondInstrument = new Instrument("Name2", 2.0, 0.2, 2.0, "22:22:22");
        Instrument thirdInstrument = new Instrument("Name3", 3.0, 0.3, 3.0, "23:23:23");
        repository.putInstrument(instrument);
        repository.putInstrument(secondInstrument);
        repository.putInstrument(thirdInstrument);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/instruments")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String expected = mapper.writeValueAsString(List.of(thirdInstrument,instrument,secondInstrument));
        assertEquals(expected,mvcResult.getResponse().getContentAsString());
    }

    @Test
    void getNewestInstrumentsDataOverride() throws Exception {
        Instrument instrument = new Instrument("Name1", 1.0, 0.1, 1.0, "11:11:11");
        repository.putInstrument(instrument);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/instruments")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String expected = mapper.writeValueAsString(List.of(instrument));
        assertEquals(expected,mvcResult.getResponse().getContentAsString());

        instrument.setTime("12.12.12");
        repository.putInstrument(instrument);
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/instruments")).andExpect(status().isOk()).andReturn();
        expected = mapper.writeValueAsString(List.of(instrument));
        assertEquals(expected,mvcResult.getResponse().getContentAsString());
    }
}